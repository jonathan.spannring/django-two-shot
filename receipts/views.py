from django.shortcuts import render, redirect
from receipts.models import Receipt
from receipts.forms import CreateReceiptForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_object": receipts,
    }
    return render(request, "receipts/receipts.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.author = request.user
            receipts.save()
            form.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)
