from django.forms import ModelForm
from receipts.models import Receipt

class CreateReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = '__all__'
